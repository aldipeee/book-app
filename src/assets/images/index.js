import LogoUvi from './LogoUvi.png';
import LogoMaiBook from './LogoMaibook.png';
import LogoIcon from './LogoIcon.png';
import LogoIconHorizontal from './LogoIconHorizontal.png';
import LogoIconHorizontalWhite from './LogoIconHorizontalWhite.png';
import CarouselBookApp from './carouselBookApp.png';
import CarouselBuy from './carouselBuy.png';
import CarouselFavorites from './carouselFavorites.png';

export {
  LogoUvi,
  LogoMaiBook,
  LogoIcon,
  LogoIconHorizontalWhite,
  CarouselBookApp,
  CarouselFavorites,
  CarouselBuy,
  LogoIconHorizontal,
};
