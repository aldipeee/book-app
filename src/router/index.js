import React, {Component} from 'react';
import {Text, View, Image, Alert, TouchableOpacity} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  Home,
  Splash,
  Favorites,
  BookDetail,
  Search,
  Login,
  Register,
  RegisterSuccess,
} from '../pages/';
import Onboarding from '../components/Onboarding';
import {BottomNavigator, CompHeaderIcon, Logout} from '../components/';
import {
  LogoIcon,
  LogoIconHorizontal,
  LogoIconHorizontalWhite,
  LogoUvi,
} from '../assets/';

import {
  WARNA_HITAM,
  WARNA_PUTIH,
  WARNA_KUNING,
  WARNA_ABUGELAP,
  WARNA_ABU_ABU,
  WARNA_DISABLE,
  WARNA_UNGU_GELAP,
  WARNA_PUTIH_UNGU,
} from '../utils/constant';

import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {logoutUser} from '../redux/actions/OtentikasiAction';

import {useNavigation} from '@react-navigation/native';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator tabBar={props => <BottomNavigator {...props} />}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Search" component={Search} />
      <Tab.Screen name="Favorites" component={Favorites} />
    </Tab.Navigator>
  );
};

const Router = props => {
  //const navigation = useNavigation();
  return (
    <Stack.Navigator
      initialRouteName="Splash"
      screenOptions={{
        headerTintColor: 'white',
        headerStyle: {backgroundColor: WARNA_HITAM},
      }}>
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Onboarding"
        component={Onboarding}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="MainApp"
        component={MainApp}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Search"
        component={Search}
        options={{
          headerTitle: () => (
            <Text style={{color: WARNA_PUTIH, fontFamily: 'Montserrat-Bold'}}>
              Search Book
            </Text>
          ),
          headerShown: true,
        }}
      />
      <Stack.Screen
        name="Favorites"
        component={Favorites}
        options={{
          headerTitle: () => (
            <Text style={{color: WARNA_HITAM, fontFamily: 'Montserrat-Bold'}}>
              Favorites
            </Text>
          ),
          headerShown: true,
        }}
      />

      <Stack.Screen
        name="BookDetail"
        component={BookDetail}
        options={{
          headerTitle: () => (
            <Text
              style={{
                color: WARNA_PUTIH,
                fontFamily: 'Montserrat-Bold',
              }}></Text>
          ),
          headerShown: true,
          headerTransparent: true,
          headerRight: () => (
            <View
              style={{
                flexDirection: 'row',
              }}>
              <CompHeaderIcon />
            </View>
          ),
        }}
      />

      <Stack.Screen
        name="Login"
        component={Login}
        options={{
          headerTitle: () => (
            <Text style={{color: WARNA_PUTIH, fontFamily: 'Montserrat-Bold'}}>
              Login
            </Text>
          ),
          headerShown: false,
        }}
      />

      <Stack.Screen
        name="Register"
        component={Register}
        options={{
          headerTitle: () => (
            <Text style={{color: WARNA_PUTIH, fontFamily: 'Montserrat-Bold'}}>
              Register
            </Text>
          ),
          headerShown: false,
        }}
      />

      <Stack.Screen
        name="RegisterSuccess"
        component={RegisterSuccess}
        options={{
          headerTitle: () => (
            <Text style={{color: WARNA_PUTIH, fontFamily: 'Montserrat-Bold'}}>
              Register Success
            </Text>
          ),
          headerShown: false,
          headerLeft: null,
        }}
      />
    </Stack.Navigator>
  );
};

// export default Router;

const mapStateToProps = state => {
  return {
    dataUser: state.OtentikasiReducer.user,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    logoutUser: () => dispatch(logoutUser()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Router);
