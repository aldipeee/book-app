import {Dimensions, StyleSheet} from 'react-native';
import {
  WARNA_ABU_ABU,
  WARNA_UTAMA,
  WARNA_HITAM,
  WARNA_KUNING,
  WARNA_PUTIH,
  WARNA_wARNING,
} from '../../utils/constant';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({
  detailMovie: {
    fontSize: 10,
    fontFamily: 'Montserrat-Regular',
    color: WARNA_PUTIH,
    marginVertical: 4,
  },
  otherDetail: {
    marginHorizontal: 20,
    top: -30,
  },
  content: {
    color: WARNA_PUTIH,
    lineHeight: 26,
  },
  title: {
    fontSize: 16,
    fontFamily: 'Montserrat-Bold',
    color: WARNA_KUNING,
    marginBottom: 10,
    marginTop: 10,
  },
  detailKiri: {paddingLeft: 15},
  detailKanan: {marginLeft: 14},
  containerCast: {},
  castName: {
    fontSize: 10,
    color: 'white',
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    textAlign: 'center',
    marginTop: 4,
  },
  namaCastSheet: {
    fontSize: 12,
    textAlign: 'center',
  },
  itemCast0: {
    width: 75,
    height: 140,
  },
  itemCast: {
    width: 75,
    height: 150,
  },
  gambarCast: {
    width: 75,
    height: 100,
    borderRadius: 4,
  },
  gambarCastBesar: {
    width: 64,
    height: 80,
    borderRadius: 4,
  },
  bottomSheetCast0: {
    flex: 1,
    width: 0.9 * windowWidth,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
  },
  bottomSheetCast: {
    flex: 1,
    width: windowWidth,
    padding: 10,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  namaCast: {
    fontSize: 10,
  },
  cast: {
    flex: 1,

    justifyContent: 'space-around',
    flexWrap: 'wrap',
  },
  backdropPath: {
    width: windowWidth,
    height: 200,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  headerTitle: {
    backgroundColor: 'white',
    width: windowWidth * 0.8,
    height: 200,
    borderRadius: 5,
    borderColor: 'grey',
    borderWidth: 1,
  },
  gambarFilm: {
    width: 85,
    height: 130,
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  loadMoreBtn: {
    padding: 10,
    backgroundColor: '#800000',
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
  },
});
