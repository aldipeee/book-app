import {StyleSheet} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
export const Styles = StyleSheet.create({
  container: {
    width: '100%',
    height: moderateScale(50),
    marginVertical: 4,
    backgroundColor: '#ebebeb',
    borderRadius: moderateScale(4),
  },
  textInput: {
    fontSize: moderateScale(15),
    marginTop: moderateScale(7),
    marginLeft: moderateScale(6),
    color: 'black',
  },
  titleStyles: {
    position: 'absolute',
    left: moderateScale(10),
    marginTop: moderateScale(3),
  },
});
