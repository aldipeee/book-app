import {ImageHeader, Logo} from '../../assets';
import React, {Component} from 'react';
import {
  Dimensions,
  View,
  Text,
  StyleSheet,
  TextInput,
  Alert,
  Button,
  ScrollView,
  StatusBar,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl,
  Sort,
} from 'react-native'; //import view dulu
import {Image} from 'react-native-elements';
import moment from 'moment';
import 'moment/locale/id';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {NavigationContainer} from '@react-navigation/native';
import {
  WARNA_ABU_ABU,
  WARNA_UTAMA,
  WARNA_HITAM,
  WARNA_KUNING,
  WARNA_PUTIH,
  WARNA_wARNING,
  WARNA_ABUGELAP,
  WARNA_UNGU_GELAP,
} from '../../utils/constant';
import InternetConnectionAlert from 'react-native-internet-connection-alert';
import {CompInternetConnectionAlert} from '../../components';
import * as Animatable from 'react-native-animatable';
import {connect} from 'react-redux';
import {registerUser} from '../../redux/actions/RegisterAction';
import {LogoUvi, LogoMaiBook, LogoIconHorizontal} from '../../assets';
import {styles} from './style';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listMovie: [],
      isLoading: false,
      valueEmail: '',
      valuePassword: '',
      valueName: '',
      hidePass: true,
      isValidEmail: true,
      isValidPassword: true,
      isValidName: true,
      erorEmail: '',
      erorPass: '',
      erorName: '',
      validToLogin: false,
    };
  }

  async componentDidMount() {
    StatusBar.setBackgroundColor(WARNA_UNGU_GELAP);
  }

  handleChangeName = valueName => {
    this.setState({valueName});
  };

  // handleButtonSubmit = async () => {
  //   if (
  //     this.state.valueEmail === '' ||
  //     this.state.valuePassword === '' ||
  //     this.state.valueName === ''
  //   ) {
  //     Alert.alert(
  //       'Eror',
  //       'Pastikan Anda sudah mengisi nama, email dan password Anda',
  //     );
  //   } else {
  //     const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  //     const hasilRegex = regex.test(this.state.valueEmail);

  //     if (!hasilRegex)
  //       Alert.alert(
  //         'Eror',
  //         'Input invalid. Input harus memenuhi format penulisan email.',
  //       );
  //     else {
  //       const dataBody = {
  //         name: this.state.valueName,
  //         email: this.state.valueEmail,
  //         password: this.state.valuePassword,
  //       };

  //       this.props.registerUser(dataBody, dataDariAction => {
  //         console.log(dataDariAction, 'Tes print data');

  //         if (dataDariAction.isRegisterSucess === true)
  //           this.props.navigation.replace('RegisterSuccess');
  //       });
  //     }
  //   }
  // };

  handleButtonSubmit = () => {
    if (
      this.state.valueEmail === '' ||
      this.state.valuePassword === '' ||
      this.state.valueName === ''
    ) {
      Alert.alert(
        'Oops, error',
        'Make sure you have input your name, email, and password correctly',
      );
    } else {
      const dataBody = {
        name: this.state.valueName,
        email: this.state.valueEmail,
        password: this.state.valuePassword,
      };

      this.props.registerUser(dataBody, dataDariAction => {
        console.log(dataDariAction, 'Tes print data');

        if (dataDariAction.isRegisterSucess === true)
          this.props.navigation.replace('RegisterSuccess');
      });
    }
  };

  showPassword = () => {
    this.setState({hidePass: !this.state.hidePass});
  };

  handleChangeEmail = valueEmail => {
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    const hasilRegex = regex.test(valueEmail);

    if (hasilRegex) {
      this.setState({isValidEmail: true});
      this.setState({valueEmail});
    } else if (!hasilRegex) {
      this.setState({isValidEmail: false});
      this.setState({erorEmail: 'Please enter a valid email address'});
    }
  };

  handleChangeName = valueName => {
    if (valueName.trim().length > 1) {
      this.setState({isValidName: true});
      this.setState({valueName});
    } else {
      this.setState({isValidName: false});
      this.setState({erorName: 'Please enter a valid name'});
    }
  };

  handleChangePassword = valuePassword => {
    const regex = /^(?=.*\d).{8,}$/;
    const hasilRegex = regex.test(valuePassword);

    if (valuePassword.trim().length < 7) {
      this.setState({valuePassword});
      this.setState({isValidPassword: false});
      this.setState({erorPass: 'Password must min 8 character'});
    } else if (!hasilRegex) {
      console.log('masuk sini');
      this.setState({isValidPassword: false});
      this.setState({erorPass: 'Make sure there is a number and a alphabet'});
    } else if (hasilRegex) {
      this.setState({isValidPassword: true});
      console.log('masuk sana');
    }

    if (valuePassword.trim().length > 7) {
      this.setState({valuePassword});
      this.setState({isValidPassword: true});
    } else {
      this.setState({isValidPassword: false});
    }
  };

  handleValidName = val => {
    if (this.state.isValidName === true) {
      this.setState({isValidName: true});
      this.setState({validToLogin: true});
    } else {
      this.setState({isValidName: false});
      this.setState({erorEmail: 'Your name is not valid'});
    }
  };

  handleValidEmail = val => {
    if (this.state.isValidEmail === true) {
      this.setState({isValidEmail: true});
      this.setState({validToLogin: true});
    } else {
      this.setState({isValidEmail: false});
      this.setState({erorEmail: 'Your email is not valid'});
    }
  };

  handleValidPass = val => {
    if (this.state.isValidPassword === true) {
      this.setState({isValidPassword: true});
      this.setState({validToLogin: true});
    } else {
      this.setState({isValidPassword: false});
      this.setState({erorPass: 'Password is not valid'});
    }
  };
  render() {
    return (
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Image
            source={LogoIconHorizontal}
            style={{width: 160, height: 30, marginBottom: 30}}
          />
          <Text style={styles.title}>Register</Text>
          <View>
            <View style={styles.form}>
              <TextInput
                style={styles.inputnama}
                placeholder="Name"
                placeholderStyle={{fontFamily: 'Karla-Regular'}}
                onChangeText={this.handleChangeName}
                onEndEditing={e => this.handleValidName(e.nativeEvent.text)}
              />
              {this.state.isValidName ? null : (
                <Animatable.View animation="fadeInLeft" duration={500}>
                  <Text
                    style={{
                      color: 'red',
                      marginBottom: 20,
                      fontFamily: 'Karla-Regular',
                    }}>
                    {this.state.erorName}
                  </Text>
                </Animatable.View>
              )}

              <TextInput
                style={styles.inputemail}
                placeholder="Email"
                keyboardType="email-address"
                placeholderStyle={{fontFamily: 'Karla-Regular'}}
                onChangeText={this.handleChangeEmail}
                onEndEditing={e => this.handleValidEmail(e.nativeEvent.text)}
              />
              {this.state.isValidEmail ? null : (
                <Animatable.View animation="fadeInLeft" duration={500}>
                  <Text style={{color: 'red', marginBottom: 20}}>
                    {this.state.erorEmail}
                  </Text>
                </Animatable.View>
              )}

              <View style={{flexDirection: 'row'}}>
                <TextInput
                  style={styles.inputPassword}
                  secureTextEntry={this.state.hidePass}
                  placeholder="Password"
                  placeholderStyle={{fontFamily: 'Karla-Regular'}}
                  name="password"
                  onChangeText={this.handleChangePassword}
                  onEndEditing={e => this.handleValidPass(e.nativeEvent.text)}
                />
                <TouchableOpacity
                  onPress={this.showPassword}
                  style={{position: 'absolute', right: 15, top: 15}}>
                  {this.state.hidePass === true ? (
                    <Icon name="eye-slash" size={20} color={WARNA_ABUGELAP} />
                  ) : (
                    <Icon name="eye" size={20} color={WARNA_ABUGELAP} />
                  )}
                </TouchableOpacity>
              </View>
              {this.state.isValidPassword ? null : (
                <Animatable.View animation="fadeInLeft" duration={100}>
                  <Text style={{color: 'red', marginBottom: 20}}>
                    {this.state.erorPass}
                  </Text>
                </Animatable.View>
              )}
              <TouchableOpacity
                style={
                  this.state.isValidEmail && this.state.isValidPassword
                    ? styles.buttonRegister
                    : styles.buttonRegisterDisabled
                }
                onPress={this.handleButtonSubmit}
                disabled={this.props.dataLoading ? true : false}>
                {this.props.dataLoading ? (
                  <ActivityIndicator size="small" color="white" />
                ) : (
                  <Text style={{color: WARNA_PUTIH, fontFamily: 'Karla-Bold'}}>
                    Sign Up
                  </Text>
                )}
              </TouchableOpacity>
            </View>
            <View style={{marginTop: 40}}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.replace('Login');
                }}>
                <Text
                  style={{
                    color: WARNA_HITAM,
                    fontFamily: 'Karla-Regular',
                    fontSize: 14,
                    marginTop: 10,
                    textAlign: 'center',
                  }}>
                  Already have an account?{'  '}
                  <Text
                    style={{fontWeight: 'bold', fontFamily: 'Karla-ExtraBold'}}>
                    Sign in
                  </Text>
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

// const mapStateToProps = state => ({});
const mapStateToProps = state => {
  return {
    dataLoading: state.OtentikasiReducer.isLoading,
  };
};
const mapDispatchToProps = {
  registerUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
