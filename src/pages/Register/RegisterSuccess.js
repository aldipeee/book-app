import {ImageHeader, Logo} from '../../assets';
import React, {Component} from 'react';
import {
  Dimensions,
  View,
  Text,
  StyleSheet,
  TextInput,
  Alert,
  Button,
  ScrollView,
  StatusBar,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl,
  Sort,
} from 'react-native'; //import view dulu
import {Image} from 'react-native-elements';
import moment from 'moment';
import 'moment/locale/id';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

import {NavigationContainer} from '@react-navigation/native';
import {
  WARNA_ABU_ABU,
  WARNA_UTAMA,
  WARNA_HITAM,
  WARNA_KUNING,
  WARNA_PUTIH,
  WARNA_wARNING,
  WARNA_UNGU_TERANG,
  WARNA_UNGU_GELAP,
  WARNA_PUTIH_UNGU,
} from '../../utils/constant';
import InternetConnectionAlert from 'react-native-internet-connection-alert';
import {CompInternetConnectionAlert} from '../../components';
import {styles} from './style';

class RegisterSuccess extends Component {
  constructor(props) {
    super(props);
  }

  async componentDidMount() {
    StatusBar.setBackgroundColor(WARNA_UNGU_GELAP);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={{color: WARNA_HITAM, fontWeight: 'bold', fontSize: 20}}>
          Registration Completed!
        </Text>
        <View
          style={{
            justifyContent: 'center',
            marginTop: 30,
            height: 180,
            alignItems: 'center',
          }}>
          <Icon
            name="check"
            size={70}
            color={WARNA_UNGU_GELAP}
            title="favorite"
          />
          <Text
            style={{
              color: WARNA_HITAM,
              marginTop: 10,
              marginBottom: 20,
              lineHeight: 20,
              textAlign: 'center',
            }}>
            We've sent email verification to your email. Please check your
            email.
          </Text>
        </View>

        <TouchableOpacity
          style={{
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            color: WARNA_PUTIH_UNGU,
            borderRadius: 4,
            backgroundColor: WARNA_UNGU_GELAP,
          }}
          title="Login"
          onPress={() => {
            this.props.navigation.replace('Login');
          }}>
          <Text style={{color: WARNA_PUTIH_UNGU}}>Log in</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default RegisterSuccess;
