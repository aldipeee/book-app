import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {Image, Button} from 'react-native-elements';
import {
  WARNA_HITAM,
  WARNA_KUNING,
  WARNA_ABUGELAP,
  WARNA_UNGU_GELAP,
} from '../../utils/constant';
import AppLayout from '../../styles/layout';
import {Colors} from '../../styles/text';
import {styles} from './style';
import {connect} from 'react-redux';
import {getBookList} from '../../redux/actions/BookAction';
import {getFavoritesList} from '../../redux/actions/FavoritesAction';
class Home extends Component {
  constructor() {
    super();
    this.state = {
      namaUser: '',
    };
  }

  componentDidMount() {
    this.props.getBookList();
  }

  _onRefresh = async () => {
    this.props.getBookList();
  };

  render() {
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={this.props.dataLoading}
            onRefresh={this._onRefresh}
          />
        }
        style={styles.container}>
        {this.props.dataLoading ? (
          <ActivityIndicator size="large" color={WARNA_UNGU_GELAP} />
        ) : (
          <View style={[AppLayout.marginHorizontalSmall, styles.body]}>
            <Text style={{color: WARNA_HITAM, marginBottom: 10}}>
              Halo, {this.props.dataUser.name.split(' ')[0]} !
            </Text>

            <Text style={styles.title}>Recommended</Text>
            <View style={styles.recommended}>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                {this.props.dataBook
                  .sort((a, b) =>
                    a.average_rating > b.average_rating ? -1 : 1,
                  )
                  .map((item, index) => (
                    <View key={index} style={styles.recommendedItem}>
                      <TouchableOpacity
                        onPress={() => {
                          this.props.navigation.navigate('BookDetail', {
                            idBook: item.id,
                          });
                        }}>
                        <View key={index} style={styles.containerBook}>
                          <Image
                            style={styles.gambarBuku}
                            source={{
                              uri: `${item.cover_image}`,
                            }}
                          />
                          <TouchableOpacity
                            onPress={() => {
                              this.props.navigation.navigate('BookDetail', {
                                idBook: item.id,
                              });
                            }}>
                            <Text
                              style={{
                                fontFamily: 'Karla-Bold',
                                color: WARNA_HITAM,
                                width: 95,
                                fontSize: 13,
                                paddingTop: 4,
                              }}>
                              {item.title.length > 20
                                ? `${item.title.substr(0, 20)}...`
                                : item.title}
                            </Text>
                          </TouchableOpacity>
                        </View>
                      </TouchableOpacity>
                    </View>
                  ))}
              </ScrollView>
            </View>

            <Text style={styles.title}>List Books</Text>
            <View>
              {this.props.dataBook.map((item, index) => (
                <View key={index} style={styles.itemContainer}>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate('BookDetail', {
                        idBook: item.id,
                      });
                    }}>
                    <Image
                      containerStyle={styles.gambarBuku}
                      source={{
                        uri: `${item.cover_image}`,
                      }}
                    />
                  </TouchableOpacity>
                  <View style={styles.itemInfoContainer}>
                    <Text numberOfLines={3} style={styles.itemTitle}>
                      {item.title}
                    </Text>
                    {/* <View
                      style={{
                        flexDirection: 'row',
                        width: '40%',
                        marginTop: 10,
                      }}>
                      <Text style={styles.itemAuthor}>{item.author}</Text>
                      <Icon name="star" size={16} color={WARNA_KUNING} />
                      <Text style={styles.itemRating}>
                        {item.average_rating}
                      </Text>
                    </View> */}
                    <Text style={styles.itemDescription}>
                      Harry Potter has no idea how famous he is. That's because
                      he's being raised by his
                    </Text>
                  </View>
                </View>
              ))}
            </View>
          </View>
        )}
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  return {
    dataBook: state.BookReducer.listBooks,
    dataUser: state.OtentikasiReducer.user,
    dataLoading: state.BookReducer.isLoading,
  };
};
const mapDispatchToProps = {
  getBookList,
  getFavoritesList,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
