import {Dimensions, StyleSheet} from 'react-native';
import {
  WARNA_ABU_ABU,
  WARNA_UTAMA,
  WARNA_HITAM,
  WARNA_KUNING,
  WARNA_wARNING,
  WARNA_PUTIH_UNGU,
  WARNA_UNGU_GELAP,
  WARNA_ABUGELAP,
} from '../../utils/constant';
import {Colors} from '../../styles/text';
import {moderateScale} from 'react-native-size-matters';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({
  body: {
    backgroundColor: '#fff',
  },
  itemContainer: {
    flexDirection: 'row',
    marginTop: 15,
    borderRadius: 7,
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    paddingBottom: 15,
    borderBottomColor: '#f0f0f0',
  },
  itemInfoContainer: {
    marginLeft: 22,
    flexGrow: 1,
    width: 0,
  },
  itemTitle: {
    fontFamily: 'KaiusPro-ExtraBoldItalic',
    color: WARNA_ABUGELAP,
    fontSize: 18,
  },
  itemAuthor: {
    color: Colors.black07,
    fontSize: 14,
    marginRight: 15,
    fontFamily: 'Karla-Medium',
  },
  itemDescription: {
    fontFamily: 'Karla-Medium',
    color: Colors.black05,
    fontSize: 15,
    marginTop: moderateScale(7),
    lineHeight: moderateScale(15),
  },

  itemRating: {
    color: Colors.black07,
    fontSize: 14,
    fontFamily: 'Karla-Medium',
    marginLeft: 5,
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerLoading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerBook: {
    paddingHorizontal: 6,
    paddingVertical: 10,
    color: WARNA_HITAM,
    flex: 1,
  },
  container: {
    paddingHorizontal: 10,
    paddingVertical: 20,
    color: WARNA_HITAM,
    backgroundColor: '#fff',
    flex: 1,
  },
  buttonShowMore: {
    backgroundColor: WARNA_KUNING,
    alignSelf: 'flex-start',
    color: WARNA_HITAM,
    fontFamily: 'Karla-Medium',
    fontSize: 12,
    padding: 6,
    borderRadius: 6,
  },
  infoMovie: {
    color: WARNA_HITAM,
  },
  posterLatest: {},
  recommended: {
    marginBottom: 20,
    color: WARNA_HITAM,
    padding: 6,
  },
  recommendedItem: {
    marginRight: 5,
  },
  movieItem: {
    padding: 6,
    paddingRight: 6,
    marginVertical: 10,
    flex: 1,
    flexDirection: 'row',
    color: WARNA_HITAM,
  },
  page: {
    flex: 1,
    padding: 10,
  },
  header: {
    width: windowWidth,
    height: windowHeight * 0.3,
    paddingHorizontal: 30,
    paddingTop: 10,
  },
  logo: {
    width: windowWidth * 0.25,
    height: windowHeight * 0.06,
  },
  hello: {
    marginTop: windowHeight * 0.03,
  },
  title: {
    fontSize: 18,
    fontFamily: 'Karla-ExtraBold',
    color: WARNA_HITAM,
    marginBottom: 8,
  },
  miniTitle: {
    fontSize: 12,
    fontFamily: 'Karla-Regular',
    color: WARNA_HITAM,
    marginBottom: 8,
  },
  username: {
    fontSize: 22,
    fontFamily: 'Karla-Bold',
  },
  label: {
    fontSize: 18,
    fontFamily: 'Karla-Bold',
    paddingBottom: 1,
  },
  garisBawah: {
    borderBottomWidth: 2,
    borderBottomColor: WARNA_UTAMA,
    width: windowWidth * 0.16,
    marginBottom: 5,
  },
  gambarBuku: {
    width: 95,
    height: 140,
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.39,
    shadowRadius: 8.3,

    elevation: 13,
  },
});
