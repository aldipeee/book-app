import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  StatusBar,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {Image} from 'react-native-elements';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {Formik} from 'formik';
import {connect} from 'react-redux';
import * as Animatable from 'react-native-animatable';
import {getUserToken} from '../../redux/actions/OtentikasiAction';
import {LogoIconHorizontal} from '../../assets';
import AppLayout from '../../styles/layout';
import AppTextStyle, {Colors} from '../../styles/text';
import TextInput from '../../components/InputFloat';
import {styles} from './style';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      hidePass: true,
      isValidEmail: true,
      isValidPassword: true,
      erorEmail: '',
      erorPass: '',
    };
  }

  componentDidMount() {
    StatusBar.setBackgroundColor(Colors.blue02);
  }

  handleButtonSubmit = dataUser => {
    this.props.getUserToken(dataUser, dataDariAction => {
      console.log(dataDariAction, 'Tes print data');
      if (dataDariAction.isLoginSucess === true) {
        this.props.navigation.replace('MainApp');
      }
    });
  };

  showPassword = () => {
    this.setState({hidePass: !this.state.hidePass});
  };

  render() {
    const formInitializeValues = {email: '', password: ''};
    return (
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Image source={LogoIconHorizontal} style={styles.logo} />
          <Text style={styles.title}>Please log in to your account</Text>
          <Formik
            initialValues={formInitializeValues}
            onSubmit={values => this.handleButtonSubmit(values)}>
            {({handleChange, handleBlur, handleSubmit, values}) => (
              <View style={styles.form}>
                <TextInput
                  title="Email"
                  titleActiveColor={Colors.black06}
                  titleInactiveColor={Colors.black05}
                  style={styles.inputUsername}
                  value={values.email}
                  placeholderStyle={AppTextStyle.secondaryFontRegular}
                  keyboardType="email-address"
                  updateMasterState={handleChange('email')}
                  onEndEditing={e => this.handleValidEmail(e.nativeEvent.text)}
                />
                {this.state.isValidEmail ? null : (
                  <Animatable.View animation="fadeInLeft" duration={500}>
                    <Text style={styles.erorEmail}>{this.state.erorEmail}</Text>
                  </Animatable.View>
                )}

                <View style={[AppLayout.row, AppLayout.marginVerticalLarge]}>
                  <TextInput
                    titleActiveColor={Colors.black06}
                    titleInactiveColor={Colors.black05}
                    title="Password"
                    style={styles.inputPassword}
                    value={values.password}
                    secureTextEntry={this.state.hidePass}
                    placeholderStyle={AppTextStyle.secondaryFontRegular}
                    name="password"
                    updateMasterState={handleChange('password')}
                    onEndEditing={e => this.handleValidPass(e.nativeEvent.text)}
                  />
                  <TouchableOpacity
                    onPress={this.showPassword}
                    style={styles.passwordEye}>
                    {this.state.hidePass === true ? (
                      <Icon name="eye-slash" size={20} color={Colors.black07} />
                    ) : (
                      <Icon name="eye" size={20} color={Colors.black07} />
                    )}
                  </TouchableOpacity>
                </View>
                {this.state.isValidPassword ? null : (
                  <Animatable.View animation="fadeInLeft" duration={100}>
                    <Text>{this.state.erorPass}</Text>
                  </Animatable.View>
                )}
                <TouchableOpacity
                  style={
                    this.state.isValidEmail && this.state.isValidPassword
                      ? styles.buttonLogin
                      : styles.buttonLoginDisabled
                  }
                  onPress={handleSubmit}
                  disabled={Boolean(this.props.dataLoading)}>
                  {this.props.dataLoading ? (
                    <ActivityIndicator size="small" color="white" />
                  ) : (
                    <Text style={styles.textButton}>Log in</Text>
                  )}
                </TouchableOpacity>
              </View>
            )}
          </Formik>

          <View style={AppLayout.marginTopLarge}>
            <Text
              style={[
                AppTextStyle.textCenter,
                AppTextStyle.secondaryFontRegular,
              ]}>
              Don't have an account?
            </Text>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('Register');
              }}>
              <Text
                style={[
                  AppTextStyle.textCenter,
                  AppTextStyle.secondaryFontRegular,
                ]}>
                Sign up
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    dataLoading: state.OtentikasiReducer.isLoading,
  };
};

const mapDispatchToProps = {
  getUserToken,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
