import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  Dimensions,
  Button,
  Share,
  TouchableOpacity,
  ScrollView,
  Linking,
  RefreshControl,
} from 'react-native';

import {
  WARNA_ABU_ABU,
  WARNA_SEKUNDER,
  WARNA_KUNING,
  WARNA_PUTIH,
  WARNA_HITAM,
  WARNA_ABUGELAP,
  WARNA_PUTIH_UNGU,
} from '../../utils/constant';
import moment from 'moment';
import 'moment/locale/id';
import axios from 'axios';
import {CompRefreshControl} from '../../components';
import {Image} from 'react-native-elements';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import NumberFormat from 'react-number-format';
import {CompInternetConnectionAlert} from '../../components';
import {styles} from './style';
import {connect} from 'react-redux';
import {getBookList, getDetailBook} from '../../redux/actions/BookAction';
import {State} from 'react-native-gesture-handler';
import StarRating from 'react-native-star-rating';

class BookDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      getIdBook: '',
    };
  }

  componentDidMount() {
    this.props.getDetailBook(this.props.route.params.idBook);
  }
  _onRefresh = async () => {
    this.props.getDetailBook(this.props.route.params.idBook);
    console.log('SUKSES REFRESH');
  };
  render() {
    console.log(
      '~~~ PRINT PROPS DETAIL BOOK DI BOOKDETAIL ~~~ ',
      this.props.dataDetailBook,
    );
    return (
      <View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={this.props.dataLoading}
              onRefresh={this._onRefresh}
            />
          }>
          <View>
            <View style={styles.containerPosterDetail}>
              <View style={styles.detailKiri}>
                <Image
                  style={styles.gambarBook}
                  source={{
                    uri: `${this.props.dataDetailBook.cover_image}`,
                  }}
                />
              </View>

              <View style={styles.detailKanan}>
                <Text
                  style={{
                    fontSize: 20,
                    fontFamily: 'Karla-ExtraBold',
                    marginBottom: 4,
                    color: WARNA_PUTIH,
                    flex: 1,
                    flexWrap: 'wrap',
                    textAlign: 'center',
                    paddingHorizontal: 15,
                    lineHeight: 30,
                  }}>
                  {this.props.dataDetailBook.title}
                </Text>
                <Text
                  style={{
                    fontFamily: 'Karla-Bold',
                    marginBottom: 8,
                    fontSize: 16,
                    color: WARNA_PUTIH,
                  }}>
                  {this.props.dataDetailBook.author}
                </Text>

                <View style={{flexDirection: 'row'}}>
                  <StarRating
                    starSize={18}
                    disabled={true}
                    maxStars={5}
                    containerStyle={{width: 90}}
                    rating={this.props.dataDetailBook.average_rating / 2}
                    fullStarColor={WARNA_KUNING}
                  />
                  <Text>{'  '}</Text>
                  <Text
                    style={{
                      color: WARNA_PUTIH,
                      marginRight: 5,
                      fontFamily: 'Karla-Regular',
                    }}>
                    {this.props.dataDetailBook.average_rating}
                  </Text>
                </View>
              </View>

              <View style={styles.detailBox}>
                <View style={styles.boxOne}>
                  <Text style={styles.miniTitle}>Sold: </Text>
                  <Text>{this.props.dataDetailBook.total_sale} books</Text>
                </View>

                <View style={styles.boxOne}>
                  <Text style={styles.miniTitle}>Stock:</Text>
                  <Text>{this.props.dataDetailBook.stock_available} books</Text>
                </View>

                <View style={styles.boxOne}>
                  <Text style={styles.miniTitle}>Buy:</Text>
                  <TouchableOpacity
                    style={{
                      fontFamily: 'Montserrat-Medium',
                      backgroundColor: WARNA_KUNING,
                      marginTop: 4,
                      paddingVertical: 4,
                      paddingHorizontal: 6,
                      borderRadius: 4,
                      alignItems: 'center',
                    }}>
                    <NumberFormat
                      value={this.props.dataDetailBook.price}
                      displayType="text"
                      thousandSeparator="."
                      decimalSeparator=","
                      prefix="Rp "
                      renderText={value => (
                        <Text style={{color: 'black'}}>{value}</Text>
                      )}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View style={styles.otherDetail}>
              <Text style={styles.title}>Synopsis</Text>
              <Text style={styles.content}>
                {this.props.dataDetailBook.synopsis}
              </Text>

              <TouchableOpacity
                style={{position: 'absolute', right: 10}}
                onPress={() => {
                  Linking.openURL(
                    `https://youtube.com/results?search_query=${this.props.dataDetailBook.title}+ book review`,
                  );
                }}>
                <Icon
                  name="youtube"
                  size={20}
                  color={WARNA_ABUGELAP}
                  title="video"
                />
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    dataBook: state.BookReducer.listBooks,
    dataUser: state.OtentikasiReducer.user,
    dataDetailBook: state.BookReducer.bookDetail,
    idDetailBook: state.BookReducer.idBook,
    dataFavoritesBook: state.FavoritesReducer.listFavorites,

    dataLoading: state.BookReducer.isLoading,
  };
};
const mapDispatchToProps = {
  getDetailBook,
};

export default connect(mapStateToProps, mapDispatchToProps)(BookDetail);
