import {Dimensions, StyleSheet} from 'react-native';
import {
  WARNA_ABU_ABU,
  WARNA_UTAMA,
  WARNA_HITAM,
  WARNA_KUNING,
  WARNA_PUTIH,
  WARNA_wARNING,
  WARNA_PUTIH_UNGU,
  WARNA_UNGU_GELAP,
  WARNA_UNGU_TERANG,
} from '../../utils/constant';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({
  miniTitle: {fontFamily: 'Karla-ExtraBold'},

  boxOne: {
    width: windowWidth / 4,
    alignItems: 'center',
  },
  detailBox: {
    width: windowWidth * 0.85,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    marginHorizontal: 30,
    backgroundColor: WARNA_PUTIH_UNGU,
    borderRadius: 4,
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginTop: 30,
  },
  otherDetail: {
    paddingVertical: 20,
    margin: 20,
  },
  content: {
    color: WARNA_HITAM,
    lineHeight: 26,
    fontFamily: 'Karla-Regular',
  },
  title: {
    fontSize: 16,
    fontFamily: 'Karla-ExtraBold',
    color: WARNA_UNGU_GELAP,
    marginBottom: 10,
    marginTop: 10,
  },
  detailKiri: {
    justifyContent: 'center', //Centered horizontally
    alignItems: 'center', //Centered vertically
    flex: 1,
  },
  detailKanan: {
    justifyContent: 'center', //Centered horizontally
    alignItems: 'center', //Centered vertically
    flex: 1,
  },
  containerPosterDetail: {
    backgroundColor: WARNA_UNGU_GELAP,
    flex: 1,
    borderBottomRightRadius: 30,
    borderBottomLeftRadius: 30,
    paddingVertical: 40,
    paddingTop: windowHeight * 0.1,
  },
  headerTitle: {
    backgroundColor: 'white',
    width: windowWidth * 0.8,
    height: 200,
    borderRadius: 5,
    borderColor: 'grey',
    borderWidth: 1,
  },
  gambarBook: {
    width: 130,
    height: 180,

    shadowColor: 'white',
    shadowOffset: {
      width: 6,
      height: 6,
    },
    shadowOpacity: 0.85,
    shadowRadius: 3.84,
    marginBottom: 16,
    borderRadius: 10,
  },
  container: {
    flex: 1,
    paddingHorizontal: 10,
    paddingVertical: 16,
    backgroundColor: WARNA_PUTIH_UNGU,
  },
  loadMoreBtn: {
    padding: 10,
    backgroundColor: '#800000',
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
  },
});
