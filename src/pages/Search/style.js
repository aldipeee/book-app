import {Dimensions, StyleSheet} from 'react-native';
import {
  WARNA_ABU_ABU,
  WARNA_UTAMA,
  WARNA_HITAM,
  WARNA_KUNING,
  WARNA_PUTIH,
  WARNA_wARNING,
  WARNA_PUTIH_UNGU,
} from '../../utils/constant';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({
  container: {
    backgroundColor: WARNA_PUTIH_UNGU,

    paddingHorizontal: 5,
    paddingVertical: 20,
  },

  latestUpload: {
    backgroundColor: WARNA_PUTIH_UNGU,
    flex: 1,
    paddingBottom: 20,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  inputBooks: {
    backgroundColor: WARNA_PUTIH,
    borderRadius: 6,
    padding: 10,
    marginBottom: 20,
    fontFamily: 'Karla-Medium',
    color: WARNA_HITAM,
  },
  buttonShowMore: {
    backgroundColor: WARNA_KUNING,
    alignSelf: 'flex-start',
    color: WARNA_HITAM,
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    padding: 6,
    borderRadius: 6,
    marginTop: 10,
  },
  author: {
    color: WARNA_PUTIH,
  },
  judul: {
    fontFamily: 'Karla-Bold',
    color: WARNA_HITAM,
    width: 95,
    fontSize: 13,
    paddingTop: 8,
  },
  infoBook: {
    marginLeft: windowWidth * 0.02,
    color: WARNA_PUTIH,
    width: 0.9 * windowWidth,
  },
  posterLatest: {},
  recommended: {
    marginBottom: 20,
    color: WARNA_PUTIH,
    padding: 6,
  },
  recommendedItem: {
    marginRight: 5,
  },
  bookItem: {
    width: 95,
    marginTop: 24,
    color: WARNA_HITAM,
    backgroundColor: WARNA_PUTIH_UNGU,
  },
  page: {
    flex: 1,
    padding: 10,
  },
  header: {
    width: windowWidth,
    height: windowHeight * 0.3,
    paddingHorizontal: 30,
    paddingTop: 10,
  },
  logo: {
    width: windowWidth * 0.25,
    height: windowHeight * 0.06,
  },
  hello: {
    marginTop: windowHeight * 0.03,
  },
  title: {
    fontSize: 18,
    fontFamily: 'Karla-Bold',
    color: WARNA_HITAM,
    marginBottom: 8,
  },
  miniTitle: {
    fontSize: 12,
    fontFamily: 'Karla-Regular',
    color: WARNA_PUTIH,
    marginBottom: 8,
  },
  username: {
    fontSize: 22,
    fontFamily: 'Karla-Bold',
  },
  layanan: {
    paddingLeft: 30,
    padding: 15,
  },
  label: {
    fontSize: 18,
    fontFamily: 'Karla-Bold',
    paddingBottom: 1,
  },
  garisBawah: {
    borderBottomWidth: 2,
    borderBottomColor: WARNA_UTAMA,
    width: windowWidth * 0.16,
    marginBottom: 5,
  },
  gambarBook: {
    width: 95,
    height: 140,
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
});
