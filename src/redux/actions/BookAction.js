import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Alert} from 'react-native';

export const GET_BOOK_LIST_START = '@@BOOK/GET_BOOK_LIST_START';
export const GET_BOOK_LIST_SUCCESS = '@@BOOK/GET_BOOK_LIST_SUCCESS';
export const GET_BOOK_LIST_FAILED = '@@BOOK/GET_BOOK_LIST_FAILED';

export const GET_BOOK_DETAIL_START = '@@BOOK/GET_BOOK_DETAIL_START';
export const GET_BOOK_DETAIL_SUCCESS = '@@BOOK/GET_BOOK_DETAIL_SUCCESS';
export const GET_BOOK_DETAIL_FAILED = '@@BOOK/GET_BOOK_DETAIL_FAILED';

export const SEARCH_BOOK_START = '@@BOOK/SEARCH_BOOK_START';
export const SEARCH_BOOK_SUCCESS = '@@BOOK/SEARCH_BOOK_SUCCESS';
export const SEARCH_BOOK_FAILED = '@@BOOK/SEARCH_BOOK_FAILED';

export const getBookList = () => async dispatch => {
  try {
    dispatch({
      type: GET_BOOK_LIST_START,
    });

    const value = await AsyncStorage.getItem('simpanTokenUser');

    //fetching awal
    const getLengthListBook = await axios.get(
      'http://code.aldipee.com/api/v1/books',
      {
        headers: {Authorization: `Bearer ${value}`},
      },
    );

    //fetching kedua
    const dataBook = await axios.get(
      `http://code.aldipee.com/api/v1/books?limit=10`,
      {
        headers: {Authorization: `Bearer ${value}`},
      },
    );

    dispatch({
      type: GET_BOOK_LIST_SUCCESS,
      dataListBook: dataBook,
    });
  } catch (error) {
    dispatch({
      type: GET_BOOK_LIST_FAILED,
    });
    Alert.alert(error.message);
  }
};

export const getDetailBook = idBook => async dispatch => {
  try {
    dispatch({
      type: GET_BOOK_DETAIL_START,
    });

    console.log('~~~ PRINT ID BOOK ~~', idBook);
    const value = await AsyncStorage.getItem('simpanTokenUser');
    const dataDetailBook = await axios.get(
      `http://code.aldipee.com/api/v1/books/${idBook}`,
      {
        headers: {Authorization: `Bearer ${value}`},
      },
    );

    dispatch({
      type: GET_BOOK_DETAIL_SUCCESS,
      dataDetailBook: dataDetailBook,
      idDetailBook: idBook,
    });
  } catch (error) {
    dispatch({
      type: GET_BOOK_DETAIL_FAILED,
    });
    Alert.alert(error.message);
  }
};

export const searchBook = keywordBook => async dispatch => {
  try {
    dispatch({
      type: SEARCH_BOOK_START,
    });

    console.log('~~~ PRINT KEYWORD BOOK ~~', keywordBook);
    const value = await AsyncStorage.getItem('simpanTokenUser');
    const fetchSearchBook = await axios.get(
      `http://code.aldipee.com/api/v1/books?title=${keywordBook}`,
      {
        headers: {Authorization: `Bearer ${value}`},
      },
    );

    console.log('~~HASIL CARI~~', fetchSearchBook);
    dispatch({
      type: SEARCH_BOOK_SUCCESS,
      dataSearchBook: fetchSearchBook,
    });
  } catch (error) {
    dispatch({
      type: SEARCH_BOOK_FAILED,
    });
    Alert.alert(error.message);
  }
};
