import axios from 'axios';
import {Alert} from 'react-native';

export const REGISTER_START = '@@BOOK/REGISTER_START ';
export const REGISTER_SUCCESS = '@@BOOK/REGISTER_SUCCESS';
export const REGISTER_FAILED = '@@BOOK/REGISTER_FAILED';

export const registerUser = (requestLogin, callbackData) => async dispatch => {
  try {
    console.log(requestLogin, 'cetak body: ');
    dispatch({
      type: REGISTER_START,
    });

    console.log('sebelum');
    const submitNewUser = await axios.post(
      'http://code.aldipee.com/api/v1/auth/register',
      requestLogin,
    );

    console.log('setelah');
    console.log(submitNewUser, 'print');

    if (submitNewUser.status === 201) {
      dispatch({
        type: REGISTER_SUCCESS,
        dataUser: submitNewUser,
      });
      //di dispatch, tetep ada parameter yg dikirim, supaya bisa kesimpan di reducer

      callbackData({isRegisterSucess: true});
    }
  } catch (error) {
    Alert.alert('Oops, Sign Up failed', 'Email has already taken');

    dispatch({
      type: REGISTER_FAILED,
    });

    // console.log(error.message);
    // Alert.alert(error.message);
  }
};
