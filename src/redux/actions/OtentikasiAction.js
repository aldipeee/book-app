import axios from 'axios';
import {Alert} from 'react-native';
import {NavigationActions} from 'react-navigation';
import {NavigationContainer} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {CommonActions} from '@react-navigation/native';

export const GET_LOGIN_TOKEN_START = '@@BOOK/GET_LOGIN_TOKEN_START ';
export const GET_LOGIN_TOKEN_SUCCESS = '@@BOOK/GET_LOGIN_TOKEN_SUCCESS';
export const GET_LOGIN_TOKEN_FAILED = '@@BOOK/GET_LOGIN_TOKEN_FAILED';
export const SET_DATA_USER = '@@BOOK/SET_DATA_USER';
export const SET_TOKEN_USER = '@@BOOK/SET_TOKEN_USER';
export const LOGOUT_START = '@@BOOK/LOGOUT_START';
export const LOGOUT_SUCCESS = '@@BOOK/LOGOUT_SUCCESS';
export const LOGOUT_FAILED = '@@BOOK/LOGOUT_FAILED';

export const getUserToken = (requestLogin, callbackData) => async dispatch => {
  try {
    dispatch({
      type: GET_LOGIN_TOKEN_START,
    });

    const fetchDataUser = await axios.post(
      'http://code.aldipee.com/api/v1/auth/login',
      requestLogin,
    );

    console.log(fetchDataUser, 'print');

    if (fetchDataUser.status === 200) {
      const simpanToken = fetchDataUser.data.tokens.access.token;

      console.log('YANG DAPET TOKENNYA: ', simpanToken);

      try {
        console.log(simpanToken, 'try');

        await AsyncStorage.setItem('simpanTokenUser', simpanToken);

        // storeData = async () => {
        //   await AsyncStorage.setItem('simpanTokenUser', simpanToken);
        // };

        // console.log('TESSS');
        // console.log('@token_user');

        // console.log(storeData, 'berhasil?');
        console.log(fetchDataUser.data.user, 'hasil loginn');

        const dataUser = fetchDataUser.data.user;
        dispatch({
          type: GET_LOGIN_TOKEN_SUCCESS,
          dataUser: dataUser,
          tokenUser: simpanToken,
        });

        callbackData({isLoginSucess: true, namaSaya: 'ony'});
      } catch (error) {
        Alert.alert(error);
      }
    }
  } catch (error) {
    dispatch({
      type: GET_LOGIN_TOKEN_FAILED,
    });
    console.log(error.message);
    // console.log(error.fetchDataUser.data.message);
    Alert.alert('Error', 'Email dan password salah!');
    // Alert.alert(error.message);
  }
};

export const setUserDataToken = (dataUser, tokenUser) => async dispatch => {
  dispatch({
    type: SET_DATA_USER,
  });
  dispatch({
    type: SET_TOKEN_USER,
  });
};

export const logoutUser = callbackData => async dispatch => {
  try {
    dispatch({
      type: LOGOUT_START,
    });

    await AsyncStorage.removeItem('simpanTokenUser');

    dispatch({
      type: LOGOUT_SUCCESS,
      tokenUser: '',
    });

    callbackData({isLogoutSucess: true});
  } catch (error) {
    dispatch({
      type: LOGOUT_FAILED,
    });
    Alert.alert(error.message);
  }
};
