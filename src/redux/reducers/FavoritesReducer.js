import {
  GET_FAVORITES_LIST_START,
  GET_FAVORITES_LIST_SUCCESS,
  GET_FAVORITES_LIST_FAILED,
  ADD_FAVORITES_START,
  ADD_FAVORITES_FAILED,
  ADD_FAVORITES_SUCCESS,
} from '../actions/FavoritesAction';

const initialState = {
  listFavorites: [],
  titlePage: 'Favorites Page',
  isLoading: false,
};

export default (state = initialState, action) => {
  console.log(action, '~~~Action yang kepanggil~~~');

  switch (action.type) {
    case GET_FAVORITES_LIST_START:
      return {...state, isLoading: true};
    case GET_FAVORITES_LIST_FAILED:
      return {...state, isLoading: false};
    case GET_FAVORITES_LIST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        listFavorites: action.dataFavoritesBook,
      };
    case ADD_FAVORITES_START:
      return {...state, isLoading: true};
    case ADD_FAVORITES_FAILED:
      return {...state, isLoading: false};
    case ADD_FAVORITES_SUCCESS: {
      return {
        ...state,
        listFavorites: action.dataFavoritesBook,
        isLoading: false,
      };
    }
    default:
      return state;
  }
};
