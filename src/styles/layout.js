import {StyleSheet} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
/**
 *
 * @param Theme can be spread like {Colors, NavigationColors, Gutters, Layout, Common, ...args}
 * @return {*}
 */
const small = 7,
  regular = 13,
  large = 15,
  superLarge = 35,
  extraLarge = 45;

export default StyleSheet.create({
  MainContainer: {
    flex: 1,
    width: moderateScale('100%'),
    height: moderateScale('100%'),
    paddingLeft: '8%',
    paddingRight: '8%',
  },

  /* Margin & Padding Layouts */
  marginVerticalExtraLarge: {
    marginVertical: moderateScale(extraLarge),
  },
  marginVerticalSuperLarge: {
    marginVertical: moderateScale(superLarge),
  },
  marginVerticalLarge: {
    marginVertical: moderateScale(large),
  },
  marginVerticalMedium: {
    marginVertical: moderateScale(regular),
  },
  marginVerticalSmall: {
    marginVertical: moderateScale(small),
  },
  marginHorizontalExtraLarge: {
    marginHorizontal: moderateScale(extraLarge),
  },
  marginHorizontalSuperLarge: {
    marginHorizontal: moderateScale(superLarge),
  },
  marginHorizontalLarge: {
    marginHorizontal: moderateScale(large),
  },
  marginHorizontalMedium: {
    marginHorizontal: moderateScale(regular),
  },
  marginHorizontalSmall: {
    marginHorizontal: moderateScale(small),
  },
  marginTopExtraLarge: {
    marginTop: moderateScale(extraLarge),
  },
  marginTopSuperLarge: {
    marginTop: moderateScale(superLarge),
  },
  marginTopLarge: {
    marginTop: moderateScale(large),
  },
  marginTopMedium: {
    marginTop: moderateScale(regular),
  },
  marginTopSmall: {
    marginTop: moderateScale(small),
  },
  marginBottomExtraLarge: {
    marginBottom: moderateScale(extraLarge),
  },
  marginBottomSuperLarge: {
    marginBottom: moderateScale(superLarge),
  },
  marginBottomLarge: {
    marginBottom: moderateScale(large),
  },
  marginBottomMedium: {
    marginBottom: moderateScale(regular),
  },
  marginBottomSmall: {
    marginBottom: moderateScale(small),
  },
  /* Column Layouts */
  column: {
    flexDirection: 'column',
  },
  columnReverse: {
    flexDirection: 'column-reverse',
  },
  colCenter: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  colVCenter: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  colHCenter: {
    flexDirection: 'column',
    justifyContent: 'center',
  },
  /* Row Layouts */
  row: {
    flexDirection: 'row',
  },
  rowReverse: {
    flexDirection: 'row-reverse',
  },
  rowCenter: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  rowVCenter: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  rowHCenter: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  /* Default Layouts */
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  alignItemsCenter: {
    alignItems: 'center',
  },
  alignItemsStart: {
    alignItems: 'flex-start',
  },
  alignItemsStretch: {
    alignItems: 'stretch',
  },
  justifyContentCenter: {
    justifyContent: 'center',
  },
  justifyContentAround: {
    justifyContent: 'space-around',
  },
  justifyContentBetween: {
    justifyContent: 'space-between',
  },
  scrollSpaceAround: {
    flexGrow: 1,
    justifyContent: 'space-around',
  },
  scrollSpaceBetween: {
    flexGrow: 1,
    justifyContent: 'space-between',
  },
  selfStretch: {
    alignSelf: 'stretch',
  },
  /* Sizes Layouts */
  fill: {
    flex: 1,
  },
  fullSize: {
    height: '100%',
    width: '100%',
  },
  fullWidth: {
    width: '100%',
  },
  fullHeight: {
    height: '100%',
  },
  /* Operation Layout */
  mirror: {
    transform: [{scaleX: -1}],
  },
  rotate90: {
    transform: [{rotate: '90deg'}],
  },
  rotate90Inverse: {
    transform: [{rotate: '-90deg'}],
  },
});
