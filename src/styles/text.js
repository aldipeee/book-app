/**
 * This file contains all application's style relative to fonts
 */
import {StyleSheet} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
/**
 *
 */
export const Colors = {
  red01: 'rgb(236,29,36)',
  red02: 'rgba(236,29,36,0.3)',
  red03: 'rgb(247,61,74)',
  red04: '#F3A3A6',
  red05: 'rgb(195, 0, 14)',
  red06: 'rgb(236,29,36)',
  green01: 'rgb(111,233,210)',
  green02: 'rgb(34,187,112)',
  green03: 'rgb(132,208,187)',
  green04: 'rgb(126,209,186)',
  green05: 'rgb(38,184,138)',
  blue01: 'rgb(159,172,230)',
  blue02: 'rgb(0, 145, 218)',
  blue03: 'rgba(0, 145, 218, 0.4)',
  blue04: 'rgb(45, 182, 210)',
  blue05: 'rgb(0, 196, 217)',
  blue06: 'rgb(24, 144, 199)',
  blue07: 'rgb(188, 224, 233)',
  blue08: 'rgb(197, 234, 243)',
  blue09: 'rgba(0, 145, 218, 0.2)',
  blue10: 'rgb(127, 157, 207)',
  blue11: 'rgba(0, 145, 218, 0.11)',
  blue12: 'rgb(102,201,225)',
  black01: 'rgb(26,22,51)',
  black02: 'rgba(0,0,0,0.15)',
  black03: 'rgba(26,22,51,0.2)',
  black04: 'rgb(54,52,55)',
  black05: 'rgb(53,51,54)',
  black06: 'rgba(29,29,38,0.5)',
  black07: 'rgba(26,22,51,0.4)',
  black08: 'rgba(26,22,51,0.1)',
  black09: 'rgba(0,0,0,0.08)',
  black10: 'rgba(26,22,51,0.5)',
  black11: 'rgb(29, 29, 31)',
  white01: 'rgb(255,255,255)',
  white02: 'rgba(255,255,255,0.8)',
  white03: 'rgba(255,255,255,0.2)',
  white04: 'rgba(255,255,255,0.01)',
  white05: 'rgb(249,249,249)',
  white06: 'rgba(255,255,255,0.4)',
  white07: 'rgba(255,255,255,0.9)',
  white08: 'rgb(245,245,245)',
  white09: '#CFEEF6',
};
export const MainResponsiveFontSize = {
  font96: moderateScale(96),
  font50: moderateScale(50),
  font40: moderateScale(40),
  font38: moderateScale(38),
  font32: moderateScale(32),
  font30: moderateScale(30),
  font28: moderateScale(28),
  font27: moderateScale(27),
  font26: moderateScale(26),
  font25: moderateScale(25),
  font24: moderateScale(24),
  font23: moderateScale(23),
  font22: moderateScale(22),
  font21: moderateScale(21),
  font20: moderateScale(20),
  font19: moderateScale(19),
  font18: moderateScale(18),
  font17: moderateScale(17),
  font16: moderateScale(16),
  font15: moderateScale(15),
  font14: moderateScale(14),
  font13: moderateScale(13),
  font12: moderateScale(12),
  font11: moderateScale(11),
  font10: moderateScale(10),
};

const FontFamily = {
  100: 'FSElliotPro-Light',
  200: 'FSElliotPro-Light',
  300: 'FSElliotPro-Regular',
  400: 'FSElliotPro-Regular',
  500: 'FSElliotPro-Bold',
  600: 'FSElliotPro-Bold',
  700: 'FSElliotPro-Bold',
  bold: 'FSElliotPro-Bold',
  800: 'FSElliotPro-Bold',
  900: 'FSElliotPro-Bold',
};
const SecondaryFont = {
  300: 'Karla-Regular',
  800: 'Karla-ExtraBold',
};

export default StyleSheet.create({
  secondaryFontRegular: {
    fontFamily: SecondaryFont['300'],
  },
  secondaryFontExtBold: {
    fontFamily: SecondaryFont['800'],
  },
  textSmall: {
    fontSize: MainResponsiveFontSize.font13,
    color: Colors.white01,
    fontFamily: FontFamily['400'],
  },
  textSmallMedium: {
    fontSize: MainResponsiveFontSize.font13,
    fontFamily: FontFamily['500'],
    color: Colors.white01,
  },
  textRegular: {
    fontSize: MainResponsiveFontSize.font15,
    fontFamily: FontFamily['400'],
    color: Colors.white01,
  },
  textRegularMedium: {
    fontSize: MainResponsiveFontSize.font15,
    fontFamily: FontFamily['500'],
    color: Colors.white01,
  },
  textLarge: {
    fontSize: MainResponsiveFontSize.font18,
    color: Colors.white01,
    fontFamily: FontFamily['400'],
  },
  textLargeMedium: {
    fontSize: MainResponsiveFontSize.font18,
    fontFamily: FontFamily['500'],
    color: Colors.white01,
  },
  titleSmall: {
    fontSize: MainResponsiveFontSize.font21,
    fontFamily: FontFamily['700'],
    color: Colors.white01,
  },
  titleRegular: {
    fontSize: MainResponsiveFontSize.font24,
    fontFamily: FontFamily['700'],
    color: Colors.white01,
    lineHeight: moderateScale(28),
  },
  titleLarge: {
    fontSize: MainResponsiveFontSize.font26,
    fontFamily: FontFamily['700'],
    color: Colors.white01,
  },
  textBold: {
    fontWeight: 'bold',
  },
  textCenter: {
    textAlign: 'center',
  },
  textJustify: {
    textAlign: 'justify',
  },
  textLeft: {
    textAlign: 'left',
  },
  textRight: {
    textAlign: 'right',
  },
  lineHeightSmall: {
    lineHeight: moderateScale(20),
  },
  lineHeightRegular: {
    lineHeight: moderateScale(24),
  },
  lineHeightLarge: {
    lineHeight: moderateScale(28),
  },
});
